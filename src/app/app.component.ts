import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { Observable } from 'rxjs/internal/Observable';
import { interval } from 'rxjs/internal/observable/interval';
import { flatMap, map, scan } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'app';
  dataStream$: Observable<any>;
  streamsCount = 1;
  vis;
  xScale;
  yScale;
  data = [1, 2, 3, 4, 5, -2, 4, 3, 3, 2, 1, 5];
  margin = {top: 20, right: 20, bottom: 20, left: 20};
  width = 1000 - this.margin.left - this.margin.right;
  height = 500 - this.margin.top - this.margin.bottom;
  xRange: ReadonlyArray<any> = [0, this.width];
  yRange: ReadonlyArray<any> = [this.height, 0];
  line;

  constructor() {
  }

  ngOnInit() {
    this.vis = d3.select('#visualisation')
      .attr('width', this.width + this.margin.left + this.margin.right)
      .attr('height', this.height + this.margin.top + this.margin.bottom)
      .append('g')
      .attr('transform', `translate(${this.margin.left},${this.margin.top})`);
    this.dataStream$ = this.genData();
    this.dataStream$.subscribe(c => {
      console.log(c);
      if (c.length >= 10) {
        this.update(c);
      }
    }, er => console.log(er));
    this.setupChart();

  }

  ngAfterViewInit() {
  }

  setupChart() {
    this.setupScales();
    this.setupAxes();

    this.line = d3.line()
      .x((d, i) => this.xScale(i))
      .y((d, i) => this.yScale(d));

    this.vis.append('path')
      .datum([])
      .attr('id', 'line')
      .attr('class', 'line')
      .attr('d', this.line)
      .transition()
      .duration(50)
      .ease(d3.easeLinear);
  }

  update(data) {
    // this.data = [...this.data, (Math.random() * 10 | 0) - 5];

    const line = d3.selectAll('#line')
      .data([data]);

    const lineEnter = line.enter()
      .append('path')
      .attr('class', 'line');

    line
      .attr('d', this.line)
      .attr('transform', null)
      .transition()
      .duration(900)
      .ease(d3.easeLinear)
      .attr('transform', `translate(${this.xScale(-1)},0)`);

    line.exit()
      .remove();

    // this.data.shift();
  }

  setupScales() {
    this.xScale = d3.scaleLinear()
      .domain([0, 10])
      .range(this.xRange);

    this.yScale = d3.scaleLinear()
      .domain([-5, 5])
      .range(this.yRange);
  }

  setupAxes() {
    const axes = this.vis.append('g')
      .attr('class', 'axes');
    axes
      .append('g')
      .attr('transform', `translate(0,${this.yScale(-5)})`)
      .call(d3.axisBottom(this.xScale));

    axes
      .append('g')
      .attr('transform', `translate(${this.xScale(0)},0)`)
      .call(d3.axisLeft(this.yScale));
  }

  addLine() {
    const line = d3.line()
      .x((d) => this.xScale(d['x'])) // set the x values for the line generator
      .y((d) => this.yScale(d['y']));

    this.vis.append('path')
      .datum(this.data)
      .attr('class', 'line')
      .attr('fill', 'none')
      .attr('stroke', 'black')
      .attr('d', line);
  }

  genData(): Observable<any> {
    return interval(1000)
      .pipe(
        flatMap(el => {
          const out = [];
          for (let i = 0; i < this.streamsCount; i++) {
            out.push({
              x: el,
              y: (Math.random() * 10 | 0) - 5
            });
          }
          return out;
        }),
        scan((acc, x) => {
          if (acc.length >= 12) {
            acc.shift();
          }
          return [...acc, x];
        }, []),
        map(el => el.map(e => e.y))
      );
  }

}
